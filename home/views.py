from django.shortcuts import render, redirect

# Create your views here.
def index(request):
    return render(request, 'index.html')

def podcast(request):
    return redirect('https://open.spotify.com/show/3UyNrS4NJaXSHFgp7L7J2H')